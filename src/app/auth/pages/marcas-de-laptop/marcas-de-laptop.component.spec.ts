import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarcasDeLaptopComponent } from './marcas-de-laptop.component';

describe('MarcasDeLaptopComponent', () => {
  let component: MarcasDeLaptopComponent;
  let fixture: ComponentFixture<MarcasDeLaptopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarcasDeLaptopComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarcasDeLaptopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
