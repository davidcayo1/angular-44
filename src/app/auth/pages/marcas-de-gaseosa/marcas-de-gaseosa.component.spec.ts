import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarcasDeGaseosaComponent } from './marcas-de-gaseosa.component';

describe('MarcasDeGaseosaComponent', () => {
  let component: MarcasDeGaseosaComponent;
  let fixture: ComponentFixture<MarcasDeGaseosaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarcasDeGaseosaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarcasDeGaseosaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
