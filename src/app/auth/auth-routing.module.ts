import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EspecialidadesComponent } from './pages/especialidades/especialidades.component';
import { CarrerasComponent } from './pages/carreras/carreras.component';
import { MarcaslaptopComponent } from './pages/marcaslaptop/marcaslaptop.component';
import { MarcasgaseosaComponent } from './pages/marcasgaseosa/marcasgaseosa.component';
import { RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'especialidades', component: EspecialidadesComponent},
      { path: 'carreras', component: CarrerasComponent},
      { path: 'laptop', component: MarcaslaptopComponent},
      { path: 'gaseosas', component: MarcasgaseosaComponent},
      { path: '**', redirectTo: 'login'}
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class AuthRoutingModule { }
